﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GetStatusModel
{
	public string server_time;
	public int code;
	public string message;
	public bool status;
	public GetStatusResponse response;
}
[System.Serializable]
public class GetStatusResponse
{
	public string backstage_version;
	public string backstage_mode;
	public string backstage_app_mode;
	public string backstage_app_version;
	public string backstage_app_version_update_status;
	public string backstage_app_mode_switch_status;
}
