﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using Newtonsoft.Json;
using UnityEngine.Networking;
using System.Threading.Tasks;
using System.Net.Http;

public class ApiManager : MonoBehaviour
{
    // Start is called before the first frame update
    private static HttpClient _httpClient = new HttpClient();
    public List<GetStatusModel> Data;

    void Start()
    {

        List<GetStatusModel> a = new List<GetStatusModel>();
        StartCoroutine(WebGetJson<GetStatusModel>(@"http://mphm.un05.com/admin/api/arcade/A0/status/2", Data));
    }

    // Update is called once per frame
    void Update()
    {
            

    }

    public IEnumerator WebGetJson<T>(string url,List<T> data) where T :class, new()
    {
        T results = default;

        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.Log(www.error);
        }
        else
        {
            
            results = JsonConvert.DeserializeObject<T>(www.downloadHandler.text);
            Debug.Log(www.downloadHandler.text);
            data.Add(results);
        }    

    }

}
